import interact from 'interactjs';
import SkillHandler from '../../components/skill/script';
import Scroller from '../../components/scroller/script';
import Clutter from '../../scripts/clutter';
import TypeWritter from '../../scripts/type-writter';
import rootObserver from '../../scripts/observers';
import {
  quadraticEaseInOutInverse,
  getTarget, setPointerNoTouch,
  setOnInteractionUp, applyAfterTransparent,
} from '../../scripts/helpers';
import { directions } from '../../scripts/enums';

export default class SkillsSectionHandler {
  constructor(skillsSection) {
    const skillsElements = skillsSection.getElementsByClassName('js-skill');
    const skillHandlers = [];
    for (let i = 0; i < skillsElements.length; i++) {
      skillHandlers.push(new SkillHandler(skillsElements[i]));
    }
    const interactable = interact('.js-draggable');
    const navContainer = skillsSection.querySelector('.js-skills-navbar-container');
    // Make the skill container transparent until is loaded
    const skillsContainer = skillsSection.querySelector('.js-skills-container');
    skillsContainer.classList.add('js-opacity-0');
    interactable.options.styleCursor = false;
    this.skillHandlers = skillHandlers;
    this.section = skillsSection;
    this.skillsContainer = skillsContainer;
    this.skillInteractable = interactable;
    this.skillsNavbar = skillsSection.querySelector('.js-skills-navbar');
    this.scroller = new Scroller(navContainer);
    this.quote = skillsSection.querySelector('.js-skills-section-quote');
    this.quoteAnimation = null;
    this.clutter = null;
    this.animationDuration = 800;
    this.state = {
      focusIterable: null,
      renderingGroup: 'none',
      animationIsStarting: false,
      skillsToRender: [],
      renderedSkills: [],
      animationStartingTime: null,
      animationRemovingSkills: false,
      animationMaxSteps: null,
      animationCurrentStep: 0,
    };
    this.skillGrabbed = null;
    this.resetId = null;
  }

  getSkillsSectionNode = () => this.section;
  getSkillHandlers = () => this.skills;

  init = () => {
    const {
      skillsContainer,
      skillHandlers,
      section,
      skillsNavbar,
      quote,
      scroller,
    } = this;
    this._setSectionHeight();
    this._setNewMarkup();

    // Set the quote
    quote.classList.add('js-quote');
    this.quoteAnimation = new TypeWritter(quote, false);
    this.quoteAnimation.setCallback(() => {
      this._toggleSkills('all');
      skillsNavbar.classList.remove('js-hidden');
      setTimeout(() => skillsNavbar.classList.remove('js-opacity-0'), 50);
      this.quoteAnimation.stop();
    });
    this.quoteAnimation.init(false);

    // Init skills handlers
    skillHandlers.forEach(val => val.init());

    // Clutter skills
    this.clutter = new Clutter(skillsContainer, skillHandlers);

    // Set skills stack (add z-index value);
    this._setSkillsStack();

    // Listen for size changes on skills
    section.addEventListener(
      'onskillresize',
      event => this._correctChildPosition(event),
    );
    // List for focus events inside section
    this.section.addEventListener('focusin', this._handleFocus);

    // Pointer events
    this.section.onpointerdown = e => setPointerNoTouch(e, this._togglePointer);
    // The pointer could go out of the element while grabbing
    // ie user try to grab outside window, therefore the listener is set
    // in the document
    document.addEventListener('pointerup', e => setPointerNoTouch(e, this._togglePointer));

    // Config interactable Listen for drag interactions
    this.skillInteractable
      .draggable({
        inertia: true,
        modifiers: [
          interact.modifiers.restrictRect({
            restriction: 'parent',
            endOnly: true,
          }),
        ],
      })
      .on('dragmove', event => this._onSkillDrag(event));

    // Listen for clicks/enter on navbar
    setOnInteractionUp(skillsNavbar, this._toggleSkillsOnSelect);

    // Create observer to popup skills and animate the quote
    rootObserver.addObservable(section, this._handleIntersection);

    // Set scroller for navbar
    scroller.init();

    // Make skills container visible
    skillsContainer.classList.remove('js-opacity-0');
  }

  reset = () => {
    const {
      resetId,
      skillsNavbar,
      skillsContainer,
      quoteAnimation,
      skillHandlers,
      state,
      scroller,
    } = this;
    const { renderingGroup } = state;

    if (resetId) {
      window.clearTimeout(resetId);
    } else {
      skillsNavbar.classList.add('js-opacity-0');
      applyAfterTransparent(skillsNavbar, 'js-hidden');
      skillsContainer.classList.add('js-opacity-0');
      if (renderingGroup !== 'none') {
        this._toggleSkills('none');
      }
      quoteAnimation.stop();
    }
    this.resetId = window.setTimeout(() => {
      this._setSectionHeight();
      skillHandlers.forEach(val => val.reset());
      // Clutter skills
      this.clutter = new Clutter(skillsContainer, skillHandlers);
      skillsContainer.classList.remove('js-opacity-0');
      quoteAnimation.start();
      this.resetId = null;

      // Reset navbar scroller
      scroller.reset();
    }, 1000);
  }

  _setNewMarkup = () => {
    const { skillsContainer } = this;
    // Remove flex and set block
    skillsContainer.classList.replace('js-static-props-section-skills', 'js-props-section-skills');
  }

  // Set the height of the section to the viewport
  // with a max of 2 * skill-container
  _setSectionHeight = () => {
    const { section } = this;
    // Get Viewport height
    const vpHeight = window.innerHeight;
    // Get max height = c-inner-container width x 2
    const maxHeight = section.firstChild.offsetWidth * 2;
    const choosenHeight = vpHeight < maxHeight ? vpHeight : maxHeight;
    // clippath remove 100 px from left edge,therefore should also be added
    const finalHeight = Math.round(100 + choosenHeight);
    this.section.style.height = `${finalHeight}px`;
  }

  _handleFocus = (e) => {
    const { skillHandlers } = this;
    const skill = getTarget(e, 'js-skill');
    if (!skill) return;
    const i = skillHandlers.findIndex(val => val.getNode().isSameNode(skill));
    if (i + 1 < skillHandlers.length) {
      this.skillHandlers.push(...skillHandlers.splice(i, 1));
      this._setSkillsStack();
    }
  }

  _setSkillsStack = () => {
    const { skillHandlers } = this;
    const maxZindex = skillHandlers.length - 1;
    skillHandlers.forEach((handler, i) => {
      handler.getNode().style.zIndex = i;
      // Only skills at the top can be expanded
      if (i < maxZindex) {
        handler.shrink();
      }
    });
  }

  _togglePointer = (e) => {
    const { skillGrabbed } = this;

    // Get the target skill
    const skill = getTarget(e, 'js-skill');

    // Change style
    if (skill && e.type === 'pointerdown') {
      skill.classList.replace('js-cursor-grab', 'js-cursor-grabbing');
      this.skillGrabbed = skill;
    } else if (skillGrabbed && e.type === 'pointerup') {
      skillGrabbed.classList.replace('js-cursor-grabbing', 'js-cursor-grab');
      this.skillGrabbed = null;
    }
  }

  _correctChildPosition = (event) => {
    const { skillsContainer } = this;
    const { target, detail } = event;

    // Get container size
    const containerW = skillsContainer.clientWidth;
    const containerH = skillsContainer.clientHeight;

    // Get skill size
    const skillWidth = target.offsetWidth;
    const skillHeight = target.offsetHeight;

    // Calculate skill edges postion
    const topEdge = target.offsetTop;
    const rightEdge = target.offsetLeft + skillWidth;
    const bottomEdge = target.offsetTop + skillHeight;
    const leftEdge = target.offsetLeft;

    // Move if out of boundaries
    let moveX = 0;
    let moveY = 0;
    if (topEdge < 0) {
      moveY = -1 * topEdge;
    } else if (bottomEdge > containerH) {
      moveY = containerH - bottomEdge;
    }
    if (leftEdge < 0) {
      moveX = -1 * leftEdge;
    } else if (rightEdge > containerW) {
      moveX = containerW - rightEdge;
    }
    if (moveX || moveY) {
      detail.move(moveX, moveY);
    }
  }

  _onSkillDrag = (e) => {
    // Get skillHandler
    const skillHandler = this._findHandler(e.target);
    if (skillHandler) skillHandler.move(e.dx, e.dy);
  }

  _toggleSkillsOnSelect = (event) => {
    if (event.type === 'keyup' && event.key !== 'Enter') return;
    const target = getTarget(event, 'js-link');
    if (target) {
      target.blur();
      let { group } = target.dataset;
      if (target.classList.contains('js-skills-section-link-active')) {
        group = 'all';
      }
      this._toggleSkills(group);
    }
  }

  _handleIntersection = (entry, dir) => {
    const { quoteAnimation, state, skillsNavbar } = this;
    const { renderingGroup } = state;
    // Quote will start when skills section intersect viewport
    // and resets when there is no intersection
    if (renderingGroup === 'none' && entry.intersectionRatio >= 0.1 && quoteAnimation.isAnimationOff()) {
      skillsNavbar.classList.add('js-opacity-0');
      applyAfterTransparent(skillsNavbar, 'js-hidden');
      quoteAnimation.start();
    // Skill will pop up if user keeps scrolling and doesn't wait until
    // typing animation finishes
    } else if (
      renderingGroup === 'none' &&
      dir.y === directions.UP &&
      entry.intersectionRatio < 0.6 &&
      entry.boundingClientRect.y <= 0
    ) {
      // Render all skills
      quoteAnimation.stop();
      this._toggleSkills('all');
      skillsNavbar.classList.remove('js-hidden');
      setTimeout(() => skillsNavbar.classList.remove('js-opacity-0'), 50);
    }
  }

  _toggleSkills = (group) => {
    const { skillsNavbar } = this;
    const links = Array.from(skillsNavbar.getElementsByClassName('js-link'));

    // Apply active style
    links.forEach((link) => {
      if (link.dataset.group === group) {
        link.classList.add('js-skills-section-link-active');
      } else {
        link.classList.remove('js-skills-section-link-active');
      }
    });

    const { renderingGroup } = this.state;
    if (group !== renderingGroup) {
      this._updateState({ renderingGroup: group, animationIsStarting: true });
      window.requestAnimationFrame(this._animateSkills);
    }
  }

  _animateSkills = (timeStamp) => {
    const { state, animationDuration, clutter } = this;
    const { renderedSkills } = state;
    let {
      animationIsStarting,
      skillsToRender,
      animationRemovingSkills,
      animationStartingTime,
      animationMaxSteps,
      animationCurrentStep,
    } = state;

    // Initialise to remove if starting and not removing already
    if (animationIsStarting && !animationRemovingSkills) {
      animationStartingTime = timeStamp;
      animationRemovingSkills = true;
      animationMaxSteps = renderedSkills.length;
      animationCurrentStep = 0;
    }

    // If rendered skills are empty, intialise the process to render new skills
    if (renderedSkills.length === 0 && animationRemovingSkills) {
      animationStartingTime = timeStamp;
      animationRemovingSkills = false;
      skillsToRender = this._getSkillsToRender();
      animationMaxSteps = skillsToRender.length;
      animationCurrentStep = 0;
    }

    animationIsStarting = false;
    // If max steps are zero there are no skills to render
    if (!animationMaxSteps) {
      return;
    }

    // Get next time
    let nextTime = quadraticEaseInOutInverse(animationCurrentStep, animationStartingTime, animationDuration, animationMaxSteps);
    // Set delay when adding skills to avoid issue:
    // last skill removed is abruptly removed
    if (!animationRemovingSkills) nextTime += 500;

    // Execute the changes if nextime
    if (nextTime <= timeStamp) {
      if (animationRemovingSkills) {
        const skill = renderedSkills.pop();
        // If skill is expanded get the shrinked version
        skill.shrink();
        skill.popdown();
      } else {
        // Get new random position  at the start
        if (animationCurrentStep === 0) clutter.clutter();
        const skill = skillsToRender.shift();
        skill.popup();
        renderedSkills.push(skill);
      }
      // update step
      animationCurrentStep += 1;
    }

    // Call recursively if there are more steps
    if (animationCurrentStep < animationMaxSteps || animationRemovingSkills) {
      window.requestAnimationFrame(this._animateSkills);
    } else {
      animationCurrentStep = 0;
      animationMaxSteps = null;
      animationStartingTime = null;
    }
    // Update state
    this._updateState({
      animationIsStarting,
      skillsToRender,
      animationRemovingSkills,
      animationStartingTime,
      animationMaxSteps,
      animationCurrentStep,
    });
  }

  _getSkillsToRender = () => {
    const { skillHandlers } = this;
    const { renderingGroup } = this.state;
    let skillsToRender;
    if (renderingGroup === 'none') {
      skillsToRender = [];
    } else if (renderingGroup === 'all') {
      skillsToRender = [...skillHandlers];
    } else {
      skillsToRender = skillHandlers.filter(skill => skill.isGroupPresent(renderingGroup));
    }
    return skillsToRender;
  }

  _updateState = (newProperties) => {
    const { state } = this;
    this.state = {
      ...state,
      ...newProperties,
    };
  }

  _findHandler = (skillNode) => {
    const { skillHandlers } = this;
    return skillHandlers.find(val => val.getNode().isSameNode(skillNode));
  }
}
