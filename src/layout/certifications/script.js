import Scroller from '../../components/scroller/script';
import CertificationHandler from '../../components/certification/script';
import hoverWatcher from '../../scripts/HoverWatcher';

export default class CertificationsSectionHandler {
  constructor(certSection) {
    const certContainer = certSection.querySelector('.js-certifications-container');
    const certElements = certSection.getElementsByClassName('js-certification');
    const certHandlers = [];
    for (let i = 0; i < certElements.length; i++) {
      certHandlers.push(new CertificationHandler(certElements[i], this._flipCert));
    }
    this.section = certSection;
    this.certContainer = certContainer;
    this.scroller = new Scroller(certContainer);
    this.certHandlers = certHandlers;
    this.state = {
      flippedCertHdl: null,
    };
  }

  init = () => {
    const { scroller, certHandlers } = this;
    scroller.init();
    // Set callback to hover watcher to add buttons when device cannot hover
    hoverWatcher.addCallback(this._toggleButtons);
    // Init certification handlers
    certHandlers.forEach(val => val.init());
  }

  reset = () => {
    const { scroller } = this;
    scroller.reset();
  }

  _toggleButtons = (pointerType) => {
    const { certHandlers } = this;
    const show = pointerType === 'touch';
    certHandlers.forEach(val => val.toggleBtn(show));
  }

  _onPointerChange = (pointerType) => {
    const { certHandlers, state } = this;
    const { flippedCertHdl } = state;
    const show = pointerType === 'touch';
    certHandlers.forEach(val => val.toggleBtn(show));
    if (flippedCertHdl && !show) this._flipCert(flippedCertHdl);
  }

  _flipCert = (certHandler) => {
    const { state } = this;
    const { flippedCertHdl } = state;
    const isShowingBack = certHandler.flip();

    if (flippedCertHdl && isShowingBack) flippedCertHdl.flip();
    this._updateState({ flippedCertHdl: isShowingBack ? certHandler : null });
  }

  _updateState = (newProps) => {
    const { state } = this;
    this.state = {
      ...state,
      ...newProps,
    };
  }
}
