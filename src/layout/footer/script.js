import messageHandler from '../../components/notification/script';
import { serialize } from '../../scripts/helpers';

export default class FooterHandler {
  constructor(footer) {
    const form = document.getElementById('user-email');
    this.footer = footer;
    this.form = form;
    this.submitBtn = form.querySelector('button');
    this.userAddress = document.getElementById('user-email-address');
    this.userEmailBody = document.getElementById('user-email-body');
    this.waitAnimation = footer.querySelector('.js-waiting');
  }

  init = () => {
    const { form, userAddress, userEmailBody } = this;
    form.onsubmit = event => this._submitEmail(event);
    userAddress.oninput = event => this._toggleBtnState(event);
    userEmailBody.oninput = event => this._toggleBtnState(event);
  }

  _submitEmail = (e) => {
    const { form, waitAnimation, submitBtn } = this;
    submitBtn.blur();
    e.preventDefault();
    const XHR = new XMLHttpRequest();
    const url = form.getAttribute('action');
    const params = serialize(form);
    XHR.open('POST', url, true);
    // Send the proper header information along with the request
    XHR.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    XHR.timeout = 30000;
    XHR.onreadystatechange = () => {
      // Call a function when the state changes.
      if (XHR.readyState !== 4) return;
      waitAnimation.classList.add('hidden');
      form.classList.remove('js-opacity-50');
      switch (true) {
      case (XHR.status === 200):
        form.reset();
        messageHandler.sendMsg('Message Sent', ['Tank you for your submission,', 'I\'ll reply a soon as possible.', 'Antonio']);
        break;
      case (XHR.status >= 500 && XHR.status < 600):
        messageHandler.sendMsg('ERROR: Message Not Sent', ['Something went wrong in our side', 'Please try later']);
        break;
      default:
        messageHandler.sendMsg('ERROR: Message Not Sent', ['I couldn\'t send the message', 'Please check your connection and try again']);
      }
      this._toggleBtnState();
    };
    XHR.send(params);
    waitAnimation.classList.remove('hidden');
    form.classList.add('js-opacity-50');
  }

  _toggleBtnState = () => {
    const { submitBtn, userAddress, userEmailBody } = this;
    if (userAddress.value !== '' && userEmailBody.value !== '') {
      submitBtn.removeAttribute('disabled');
    } else {
      submitBtn.setAttribute('disabled', 'true');
    }
  }
}
