import rootObserver from '../../scripts/observers';
import TopbarHandler from './topbar';

export default class NavbarHandler {
  constructor(navbar) {
    const topbar = document.getElementById('topbar');
    if (topbar) this.topbarHandler = new TopbarHandler(topbar);
    this.sectionMarkers = [...navbar.getElementsByClassName('js-section-link')]
      .reduce((acc, secPointer) => {
        const { sectionId } = secPointer.dataset;
        acc[sectionId] = secPointer.querySelector('.js-section-marker');
        return acc;
      }, {});
    this.underline = navbar.querySelector('.js-main-underline');
    this.navbar = navbar;
    this.observer = rootObserver;
    this.state = {
      visibleSectionId: null,
      sectionVisibleHeight: 0,
      sticked: null,
    };
  }

  getNavbarNode = () => this.navbar;

  init = () => {
    const { observer, navbar, sectionMarkers, topbarHandler } = this;
    if (topbarHandler) topbarHandler.init();
    observer.addObservable(navbar, this._setStyle);
    Object.keys(sectionMarkers).forEach((sectionId) => {
      const section = document.getElementById(sectionId);
      observer.addObservable(section, entry => this._toggleSection(sectionId, entry));
    });
  }

  _setStyle = (entry) => {
    const { topbarHandler, navbar } = this;
    const sectionMarkers = Object.values(this.sectionMarkers);
    const topbarStyle = window.getComputedStyle(topbarHandler.getDOMNode());
    const isTopbarActive = topbarStyle.getPropertyValue('display') === 'flex';
    // TOPBAR PRESENCE
    // If navbar in viewport and not in header => hide topbar
    if (entry.intersectionRatio >= 0.1 || entry.boundingClientRect.y > 0) {
      topbarHandler.hide();
      navbar.classList.replace('js-opacity-0', 'js-opacity-1');
      navbar.classList.replace('-js-z-10', 'js-z-30');
    // Else => show topbar
    } else if (isTopbarActive) {
      topbarHandler.show();
      navbar.classList.replace('js-opacity-1', 'js-opacity-0');
      navbar.classList.replace('js-z-30', '-js-z-10');
    }
    // UNDERLINE STYLE
    // If not sticked => show underline
    if (entry.intersectionRatio >= 0.45) {
      this.underline.classList.replace('js-opacity-0', 'js-opacity-100');
      // Remove marker border from pointers
      sectionMarkers.forEach(
        pointer => pointer.classList.replace('js-opacity-100', 'js-opacity-0'),
      );
      this._updateState({
        visibleSectionId: null,
        sectionVisibleHeight: 0,
        sticked: false,
      });
    // Else = remove underline (underline of specific section managed by toggleSection)
    } else {
      this.underline.classList.replace('js-opacity-100', 'js-opacity-0');
      this._updateState({
        sticked: true,
      });
    }
  }

  _toggleSection = (sectionId, entry) => {
    const { sectionVisibleHeight, visibleSectionId, sticked } = this._getState();
    // If navbar is not in the top position return
    if (!sticked) return;
    const entrySecPointer = this.sectionMarkers[sectionId];
    const visibleSectionPointer = this.sectionMarkers[visibleSectionId];
    let newSectVisH = sectionVisibleHeight;
    let newVisibleSectionId = visibleSectionId;

    // If entry visible intersect rectangle higher than current one set new rect height and visibleSection section
    // else if section is the same as visible section update state visible height
    if (entry.intersectionRect.height > newSectVisH) {
      newSectVisH = entry.intersectionRect.height;
      newVisibleSectionId = sectionId;
    } else if (sectionId === newVisibleSectionId) {
      newSectVisH = entry.intersectionRect.height;
    }

    // If state current section is different than new visible section update the marker
    if (newVisibleSectionId !== visibleSectionId) {
      entrySecPointer.classList.replace('js-opacity-0', 'js-opacity-100');
      if (visibleSectionPointer) {
        visibleSectionPointer.classList.replace('js-opacity-100', 'js-opacity-0');
      }
    }

    this._updateState({
      visibleSectionId: newVisibleSectionId,
      sectionVisibleHeight: newSectVisH,
    });
  }

  _updateState = (newProperties) => {
    const { state } = this;
    this.state = {
      ...state,
      ...newProperties,
    };
  }

  _getState = () => this.state;
}
