import rootObserver from '../../scripts/observers';
import { applyAfterTransparent, setOnInteractionUp } from '../../scripts/helpers';

export default class TopbarHandler {
  constructor(topbar) {
    this.sections = [...topbar.getElementsByClassName('js-section-link')]
      .reduce((acc, secPointer) => {
        const { sectionId } = secPointer.dataset;
        acc[sectionId] = secPointer.querySelector('a');
        return acc;
      }, {});
    this.topbar = topbar;
    this.checkBox = topbar.querySelector('.js-topbar-checkbox');
    this.observer = rootObserver;
    this.state = {
      isTopVisible: false,
      isNavVisible: false,
      visibleSectionId: null,
      sectionVisibleHeight: 0,
    };
  }

  getDOMNode = () => this.topbar;

  init = () => {
    const { observer, sections, checkBox } = this;
    Object.entries(sections).forEach((section) => {
      const [sectionId, aNode] = section;
      // Add observable for every section
      const sectionNode = document.getElementById(sectionId);
      observer.addObservable(sectionNode, entry => this._setSectionName(sectionId, entry));
      // Add eventlistener to close nav
      setOnInteractionUp(aNode, this._closeNav);
    });
    // Listen for changes on checkbox
    checkBox.addEventListener('change', event => this._toggleNav(event));
  }

  show = () => {
    this._updateState({ isTopVisible: true });
  }

  hide = () => {
    this._updateState({ isTopVisible: false });
  }

  _closeNav = () => {
    const { checkBox } = this;
    checkBox.checked = false;
    this._toggleNav();
  }

  _setSectionName = (sectionId, entry) => {
    const { topbar, sections } = this;
    const sectionText = topbar.querySelector('.js-topbar-text');
    const { sectionVisibleHeight, visibleSectionId } = this._getState();

    let newSectVisH = sectionVisibleHeight;
    let newVisibleSectionId = visibleSectionId;

    // If entry visible intersect rectangle higher than current one set new rect height and visibleSection section
    // else if section is the same as visible section update state visible height
    if (entry.intersectionRect.height > newSectVisH) {
      newSectVisH = entry.intersectionRect.height;
      newVisibleSectionId = sectionId;
    } else if (sectionId === newVisibleSectionId) {
      newSectVisH = entry.intersectionRect.height;
    }

    // If state current section is different than new visible section update the text
    if (newVisibleSectionId !== visibleSectionId) {
      sectionText.textContent = sections[sectionId].textContent;
    }

    this._updateState({
      visibleSectionId: newVisibleSectionId,
      sectionVisibleHeight: newSectVisH,
    });
  }

  _toggleNav = () => {
    const { topbar, checkBox } = this;
    const bg = topbar.querySelector('.js-topbar-btn-bg');
    if (checkBox.checked) {
      bg.classList.remove('js-hidden');
      window.setTimeout(() => bg.classList.replace('js-opacity-0', 'js-opacity-1'), 10);
      this._updateState({ isNavVisible: true });
    } else {
      bg.classList.replace('js-opacity-1', 'js-opacity-0');
      applyAfterTransparent(bg, 'js-hidden');
      this._updateState({ isNavVisible: false });
    }
  }

  _updateState = (newProperties) => {
    const { state } = this;
    this.state = {
      ...state,
      ...newProperties,
    };
    this._onStateChange();
  }

  _onStateChange = () => {
    const { topbar, state } = this;
    const { isTopVisible, isNavVisible } = state;
    // If any visible keep nav or top bar in the same position
    if (isTopVisible || isNavVisible) {
      topbar.classList.remove('-js-translate-y-full');
    } else {
      topbar.classList.add('-js-translate-y-full');
    }
  }

  _getState = () => this.state;
}
