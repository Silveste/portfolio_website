import ParallaxAnimation from '../../scripts/parallax-translator';
import { axes } from '../../scripts/enums';

const { YAXIS } = axes;

export default class HeaderHandler {
  constructor(header, parallax = true) {
    // Parallax scrolling
    let parallaxAnimation;
    if (parallax) {
      parallaxAnimation = new ParallaxAnimation(header);
      const bg = header.querySelector('.js-header-bg');
      parallaxAnimation.animateElement(bg, YAXIS, 0.5);
      const terminal = header.querySelector('.js-terminal');
      parallaxAnimation.animateElement(terminal, YAXIS, 0.4);
      const avatar = header.querySelector('.js-avatar');
      parallaxAnimation.animateElement(avatar, YAXIS, 0.3, true);
      const icons = header.querySelector('.js-social-icons');
      parallaxAnimation.animateElement(icons, YAXIS, 0.3);
    }

    // Class properties
    this.header = header;
    this.parallaxAnimation = parallaxAnimation;

    this.mediaMinWidthForParallax = '(min-width: 768px)';
  }

  getHeaderNode = () => this.header;

  init() {
    const { parallaxAnimation, mediaMinWidthForParallax } = this;
    if (parallaxAnimation && window.matchMedia(mediaMinWidthForParallax).matches) {
      parallaxAnimation.init();
    }
  }

  reset() {
    const { parallaxAnimation, mediaMinWidthForParallax } = this;
    if (parallaxAnimation && window.matchMedia(mediaMinWidthForParallax).matches) {
      parallaxAnimation.start();
    } else {
      parallaxAnimation.stop();
    }
  }
}
