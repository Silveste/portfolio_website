import TypeWritter from '../../../scripts/type-writter';
import rootObserver from '../../../scripts/observers';
import { watchElement } from '../../../scripts/helpers';

export default class TerminalHandler {
  constructor(terminal, onlyScreenLinesClassName = 'js-only-loading') {
    const terminalBody = terminal.querySelector('.js-terminal-body');
    const terminalAnimation = new TypeWritter(terminalBody, true, 'js-header-cursor');
    this.terminal = terminal;
    this.terminalBody = terminalBody;
    this.terminalAnimation = terminalAnimation;
    this.onlyScreenLinesClassName = onlyScreenLinesClassName;
    this.loadingScreenMaxTime = Number(terminal.dataset.max);
    this.loadingScreenMinTime = Number(terminal.dataset.min);
    this.state = {
      headerTerminal: true,
    };
  }

  init() {
    const { terminalAnimation, terminal, loadingScreenMinTime } = this;
    rootObserver.addObservable(terminal, this._toggleIdle);
    terminalAnimation.init();
    // While loading document, set terminal at full screen (window)
    this._setLoadingScreen();
    // After min time at full screen watch loading status
    window.setTimeout(this._watchUpdates.bind(this), loadingScreenMinTime);
  }

  _setLoadingScreen() {
    const { terminal, onlyScreenLinesClassName, state } = this;
    const { headerTerminal } = state;
    if (!headerTerminal) return;
    this.state.headerTerminal = false;
    // Prevent page scrolling
    const body = document.querySelector('body');
    body.classList.add('js-h-full', 'js-overflow-hidden');
    // Maximise terminal to the entire screen
    terminal.classList.replace('js-header-terminal', 'js-main-terminal');
    // Show lines that render only in the loading screen;
    const hiddenLines = terminal.getElementsByClassName(onlyScreenLinesClassName);
    Array.from(hiddenLines).forEach(el => el.classList.remove('js-hidden'));
  }

  _setHeaderTerminal() {
    const { terminal, onlyScreenLinesClassName, state } = this;
    const { headerTerminal } = state;
    if (headerTerminal) return;
    this.state.headerTerminal = true;
    // Animate the transition
    const terminalComputedStyle = window.getComputedStyle(terminal);
    terminal.style.opacity = '0';
    terminal.style.transform += ' scale(0)';
    watchElement(
      terminal,
      // resolveIf
      () => terminalComputedStyle.getPropertyValue('opacity') === '0',
    )
      .finally(() => {
        // Allow scrolling
        const body = document.querySelector('body');
        body.classList.remove('js-h-full', 'js-overflow-hidden');
        // Reduce terminal to its original position
        terminal.classList.replace('js-main-terminal', 'js-header-terminal');
        // Hide lines that render only in the loading screen;
        const hiddenLines = terminal.getElementsByClassName(onlyScreenLinesClassName);
        Array.from(hiddenLines).forEach(el => el.classList.add('js-hidden'));
        // Scale the terminal to it's original size
        window.requestAnimationFrame(() => {
          terminal.style.transform = terminal.style.transform.replace('scale(0)', '');
          terminal.style.opacity = '1';
        });
      });
  }

  _watchUpdates() {
    const { loadingScreenMaxTime: max, loadingScreenMinTime: min } = this;
    // if page is fully loaded set the terminal in the header
    if (document.readyState === 'complete') {
      this._setHeaderTerminal();
    } else {
      window.addEventListener('load', this._setHeaderTerminal.bind(this), { once: true });
    }
    // If reached max loading time set also terminal in the header
    window.setTimeout(this._setHeaderTerminal.bind(this), (max - min));
  }

  _toggleIdle = (entry) => {
    const { terminalAnimation } = this;
    if (entry.isIntersecting) {
      terminalAnimation.start();
    } else {
      terminalAnimation.stop(false);
    }
  }
}
