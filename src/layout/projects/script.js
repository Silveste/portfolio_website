import Scroller from '../../components/scroller/script';
import ProjectHandler from '../../components/project/script';
import hoverWatcher from '../../scripts/HoverWatcher';

export default class ProjectsSectionHandler {
  constructor(projectsSection) {
    const projectsContainer = projectsSection.querySelector('.js-projects-container');
    const projectElements = projectsSection.getElementsByClassName('js-project');
    const projectHandlers = [];
    for (let i = 0; i < projectElements.length; i++) {
      projectHandlers.push(new ProjectHandler(projectElements[i], this._flipProject));
    }
    this.section = projectsSection;
    this.projectsContainer = projectsContainer;
    this.projectHandlers = projectHandlers;
    this.scroller = new Scroller(projectsContainer);
    this.state = {
      flippedProjectHdl: null,
    };
  }

  init = () => {
    const { scroller, projectHandlers } = this;
    scroller.init();
    // Set callback to hover watcher to add buttons when device cannot hover
    hoverWatcher.addCallback(this._onPointerChange);
    // Init certification handlers
    projectHandlers.forEach(val => val.init());
  }

  reset = () => {
    const { scroller } = this;
    scroller.reset();
  }

  _onPointerChange = (pointerType) => {
    const { projectHandlers, state } = this;
    const { flippedProjectHdl } = state;
    const show = pointerType === 'touch';
    projectHandlers.forEach(val => val.toggleBtn(show));
    if (flippedProjectHdl && !show) this._flipProject(flippedProjectHdl);
  }

  _flipProject = (projectHandler) => {
    const { state } = this;
    const { flippedProjectHdl } = state;
    const isShowingBack = projectHandler.flip();
    if (flippedProjectHdl && isShowingBack) flippedProjectHdl.flip();
    this._updateState({ flippedProjectHdl: isShowingBack ? projectHandler : null });
  }

  _updateState = (newProps) => {
    const { state } = this;
    this.state = {
      ...state,
      ...newProps,
    };
  }
}
