/*
CONVENTION: JS Create classes to export. Constructor is executed at the beginning and init method when window loads
CONVENTION: JS: Use Id selectors only in main-script.js to pass section elements into section handlers
CONVENTION: JS: To select elements inside components/layout use sectionElement.methodToSelect
CONVENTION JS: When JS changes styles use classes with styles. If the value is continuous use inline styles
CONVENTION: JS: Apply styles using classes starting with JS- (whitelisted from PurgeCSS). If neccessary include them in js-handlers.css
CONVENTION: JS, CSS animations should be considered first than JS animation.
*/

import './main-styles';
import NavbarHandler from './layout/navbar/navbar';
import HeaderHandler from './layout/header/script';
import TerminalHandler from './layout/header/terminal/script';
import SkillsSectionHandler from './layout/skills/script';
import ProjectsSectionHandler from './layout/projects/script';
import CertificationsSectionHandler from './layout/certifications/script';
import FooterHandler from './layout/footer/script';
import hoverWatcher from './scripts/HoverWatcher';
import { watchElement } from './scripts/helpers';

window.addEventListener('DOMContentLoaded', () => {
  // Scroll page up (Prevents weird positioning of terminal onLoad)
  window.scrollTo(0, 0);
  // Select DOM components and instantiate handlers
  // LOADING-SCREEN && HEADER TERMINAL
  const terminal = document.querySelector('#terminal');
  const terminalHandler = new TerminalHandler(terminal);
  // NAVBAR
  const navbar = document.querySelector('#navbar');
  const navbarHandler = new NavbarHandler(navbar);
  // HEADER
  const header = document.querySelector('#header');
  const headerHandler = new HeaderHandler(header);
  // SECTION SKILLS
  const skills = document.querySelector('#skills');
  const skillsSectionHandler = new SkillsSectionHandler(skills);
  // SECTION PROJECTS
  const projects = document.querySelector('#projects');
  const projectsSectionHandler = new ProjectsSectionHandler(projects);
  // SECTION CERTIFICATIONS
  const certifications = document.querySelector('#certifications');
  const certificationsSectionHandler = new CertificationsSectionHandler(certifications);
  // FOOTER
  const footer = document.querySelector('#footer');
  const footerHandler = new FooterHandler(footer);


  // Initialize handlers
  navbarHandler.init();
  headerHandler.init();
  terminalHandler.init();
  skillsSectionHandler.init();
  projectsSectionHandler.init();
  certificationsSectionHandler.init();
  footerHandler.init();
  hoverWatcher.init();

  // Some sections need reseting after resizing the window horizontally
  let windowWidth = window.innerWidth;
  window.onresize = () => {
    if (windowWidth !== window.innerWidth) {
      headerHandler.reset();
      skillsSectionHandler.reset();
      projectsSectionHandler.reset();
      certificationsSectionHandler.reset();
      windowWidth = window.innerWidth;
    }
  };

  // Message for curious developers
  console.log('Hmmmmm... 🤔🤔🤔🤔🤔 \nif you\'re here, you might want to visit the repostory:\nhttps://gitlab.com/Silveste/portfolio_website');

  // Remove spinner from DOM
  const spinner = document.querySelector('#initial-spinner');
  spinner.style.opacity = 0;
  const spComputed = window.getComputedStyle(spinner);
  watchElement(spinner, () => spComputed.getPropertyValue('opacity') === '0')
    .finally(() => spinner.remove());
}, { once: true });
