import interact from 'interactjs';
import { directions, axes } from '../../scripts/enums';
import { setPointerNoTouch } from '../../scripts/helpers';

const { UP, DOWN, LEFT, RIGHT, NODIR, ALLDIR } = directions;
const { XAXIS, YAXIS } = axes;

export default class Scroller {
  constructor(
    container,
    handlers = true,
    axis = XAXIS,
    nonHandlerTransparencyEmphasis = 3.5,
    naturalScroll = true,
    cursorActive = true,
    initialSpeed = 5,
    acceleration = 0.35,
    deceleration = 0.65,
    scrollableRowSelector = '.js-scroll',
    negativeArrowSelector = '.js-left-arrow',
    positiveArrowSelector = '.js-right-arrow',
    arrowHiddenStylesClass = 'js-hidden',
    cursorArrowUpStylesClass = 'js-cursor-t-scroll',
    cursorArrowDownStylesClass = 'js-cursor-b-scroll',
    cursorDoubleYStylesClass = 'js-cursor-y-scroll',
    cursorArrowLeftStylesClass = 'js-cursor-l-scroll',
    cursorArrowRightStylesClass = 'js-cursor-r-scroll',
    cursorDoubleXStylesClass = 'js-cursor-x-scroll',
  ) {
    const row = container.querySelector(scrollableRowSelector);
    const cursorMap = {};
    cursorMap[UP] = cursorArrowUpStylesClass;
    cursorMap[DOWN] = cursorArrowDownStylesClass;
    cursorMap[ALLDIR] = axis === XAXIS ? cursorDoubleXStylesClass : cursorDoubleYStylesClass;
    cursorMap[LEFT] = cursorArrowLeftStylesClass;
    cursorMap[RIGHT] = cursorArrowRightStylesClass;
    const leftOffset = row.firstElementChild.offsetLeft;
    const rightOffset = row.scrollWidth - (row.lastElementChild.offsetLeft + row.lastElementChild.offsetWidth);
    const topOffset = row.firstElementChild.offsetTop;
    const bottomOffset = row.scrollHeight - (row.lastElementChild.offsetTop + row.lastElementChild.offsetHeight);
    this.container = container;
    this.row = row;
    if (handlers) {
      this.arrowNeg = container.querySelector(negativeArrowSelector);
      this.arrowPos = container.querySelector(positiveArrowSelector);
    }
    this.handlers = handlers;
    this.borderSize = nonHandlerTransparencyEmphasis;
    this.startOffset = axis === XAXIS ? leftOffset : topOffset;
    this.endOffset = axis === XAXIS ? rightOffset : bottomOffset;
    this.initialSpeed = initialSpeed;
    this.acceleration = acceleration;
    this.deceleration = deceleration;
    this.natural = naturalScroll;
    this.cursorActive = cursorActive;
    this.axis = axis;
    this.cursorMap = cursorMap;
    this.arrowHiddenStylesClass = arrowHiddenStylesClass;
    this.interactable = interact(row);
    this.state = {
      scrollingTo: NODIR,
      trigger: null,
      stopTriggerByElementActiveStatus: null,
      speed: 0,
      activeScrollId: 0,
    };
  }

  init = () => {
    const { interactable, row, axis } = this;
    interactable.options.styleCursor = false;
    interactable
      .draggable({
        inertia: true,
        origin: 'self',
        startAxis: axis,
        lockAxis: axis,
        modifiers: [
          interact.modifiers.restrict({
            restriction: 'self',
          }),
        ],
      })
      .on('dragmove', (event) => {
        const step = axis === YAXIS ? event.dy : event.dx;
        this.moveTo(-1, step);
      })
      // Prevents scrolling when dragging the mouse out of the area
      .on('down', (event) => {
        event.preventDefault();
        row.focus();
      });
    // Set initial state
    this._updateScrollingDirs();
    // Add listeners
    row.addEventListener('keydown', event => this._onArrowKeyPress(event));
    row.addEventListener('keyup', event => this._onArrowKeyPress(event));
    row.addEventListener('scroll', () => this._updateScrollingDirs());
    row.addEventListener('wheel', event => this._onwheel(event));
  }

  reset = () => {
    const { handlers } = this;
    this.resetState();
    this._toggleCursor();
    if (handlers) this._updateHandlers();
    this._setTransparentBorders();
    this._updateScrollingDirs();
  }

  _updateScrollingDirs = () => {
    const { axis, row, natural, state, handlers } = this;
    const { scrollingTo } = state;
    const xDirs = {
      max: natural ? RIGHT : LEFT,
      center: ALLDIR,
      min: natural ? LEFT : RIGHT,
    };
    const yDirs = {
      max: natural ? DOWN : UP,
      center: ALLDIR,
      min: natural ? UP : DOWN,
    };
    let pos;
    let posMax;
    let dirs;
    if (axis === XAXIS) {
      pos = row.scrollLeft;
      posMax = row.scrollWidth - row.clientWidth;
      dirs = { ...xDirs };
    } else {
      pos = row.scrollTop;
      posMax = row.scrollHeight - row.clientHeight;
      dirs = { ...yDirs };
    }
    // Posmax = 0 means row is shorter than space available therefore no need for scrolling
    let newScrollingTo;
    if (posMax === 0) {
      newScrollingTo = NODIR;
    } else {
      switch (pos) {
      case 0:
        newScrollingTo = dirs.min;
        this.stopScroll();
        break;
      case posMax:
        newScrollingTo = dirs.max;
        this.stopScroll();
        break;
      default:
        newScrollingTo = dirs.center;
      }
    }

    // If state changes
    if (newScrollingTo !== scrollingTo) {
      // Save new state
      this._saveState({ scrollingTo: newScrollingTo });
      // Set the cursor
      this._toggleCursor();
      // Activate Arrow handlers
      if (handlers) this._updateHandlers();
      // Set transparent borders
      this._setTransparentBorders();
    }
  }

  _saveState = (newStateProps) => {
    const { state } = this;
    this.state = {
      ...state,
      ...newStateProps,
    };
  }

  resetState = () => {
    this._saveState({
      scrollingTo: NODIR,
      trigger: null,
      stopTriggerByElementActiveStatus: null,
      speed: 0,
      activeScrollId: this._getScrollId(),
    });
  }

  _getScrollId = () => Math.round(Math.random() * 1000000);

  _toggleCursor = () => {
    const { row, state, cursorMap } = this;
    row.classList.remove(...Object.values(cursorMap));
    if (state.scrollingTo !== NODIR) {
      row.classList.add(cursorMap[state.scrollingTo]);
    }
  }

  _updateHandlers = () => {
    const {
      arrowNeg,
      arrowPos,
      arrowHiddenStylesClass,
      state,
    } = this;
    const { scrollingTo } = state;

    // Remove all styles and disable the buttons
    arrowNeg.classList.remove(arrowHiddenStylesClass);
    arrowPos.classList.remove(arrowHiddenStylesClass);
    arrowPos.disabled = true;
    arrowNeg.disabled = true;
    // Remove listeners
    arrowPos.onpointerdown = null;
    arrowNeg.onpointerdown = null;
    arrowPos.ontouchstart = null;
    arrowNeg.ontouchstart = null;
    arrowPos.ontouchend = null;
    arrowNeg.ontouchend = null;

    switch (scrollingTo) {
    case NODIR:
      arrowNeg.classList.add(arrowHiddenStylesClass);
      arrowPos.classList.add(arrowHiddenStylesClass);
      break;
    case DOWN:
    case RIGHT:
      arrowPos.disabled = false;
      arrowPos.ontouchstart = this._onArrowHandlerDown;
      arrowPos.ontouchend = this.stopScroll;
      arrowPos.onpointerdown = e => setPointerNoTouch(e, this._onArrowHandlerDown);
      break;
    case UP:
    case LEFT:
      arrowNeg.disabled = false;
      arrowNeg.ontouchstart = this._onArrowHandlerDown;
      arrowNeg.ontouchend = this.stopScroll;
      arrowNeg.onpointerdown = e => setPointerNoTouch(e, this._onArrowHandlerDown);
      break;
    // Default both directions
    default:
      arrowPos.disabled = false;
      arrowNeg.disabled = false;
      arrowPos.ontouchstart = this._onArrowHandlerDown;
      arrowPos.ontouchend = this.stopScroll;
      arrowPos.onpointerdown = e => setPointerNoTouch(e, this._onArrowHandlerDown);
      arrowNeg.ontouchstart = this._onArrowHandlerDown;
      arrowNeg.ontouchend = this.stopScroll;
      arrowNeg.onpointerdown = e => setPointerNoTouch(e, this._onArrowHandlerDown);
    }
  };

  _setTransparentBorders = () => {
    const {
      handlers,
      startOffset,
      endOffset,
      axis,
      row,
      state,
      natural,
      borderSize,
    } = this;
    const { scrollingTo } = state;
    // If there is no handlers the transition to transparent will be wider
    const startTransparencySize = handlers ? startOffset : Math.round(startOffset * borderSize);
    const endTransparencySize = handlers ? endOffset : Math.round(endOffset * borderSize);

    let direction;
    if (axis === XAXIS) {
      direction = natural ? 'to left' : 'to right';
    } else {
      direction = natural ? 'to top' : 'to bottom';
    }
    let maskImage;
    switch (scrollingTo) {
    case NODIR:
      maskImage = '';
      break;
    case DOWN:
    case RIGHT:
      maskImage = `linear-gradient(${direction}, #000 ${row.clientWidth - endTransparencySize}px, transparent ${row.clientWidth - 5}px)`;
      break;
    case UP:
    case LEFT:
      maskImage = `linear-gradient(${direction}, transparent 5px, #000 ${startTransparencySize}px)`;
      break;
    // Default both directions
    default:
      maskImage = `linear-gradient(${direction}, transparent 5px, #000 ${startTransparencySize}px ${row.clientWidth - endTransparencySize}px, transparent ${row.clientWidth - 5}px)`;
    }
    row.style.maskImage = maskImage;
    row.style.WebkitMaskImage = maskImage;
  }

  _onArrowKeyPress = (event) => {
    const { axis } = this;
    const keys = [];
    if (axis === XAXIS) {
      keys.push('ArrowRight', 'ArrowLeft');
    } else {
      keys.push('ArrowDown', 'ArrowUp');
    }
    const { trigger } = this.state;
    if (!keys.includes(event.key)) return;
    if (event.type === 'keyup') {
      this.stopScroll();
      return;
    }
    event.preventDefault();
    if (!trigger) {
      const newState = {
        trigger: event,
        stopTriggerByElementActiveStatus: false,
      };
      let dir;
      if (keys.indexOf(event.key) === 0) {
        dir = 1;
      } else {
        dir = -1;
      }
      this._startScroll(dir, newState);
    }
  }

  _onArrowHandlerDown = (event) => {
    const { arrowNeg, arrowPos } = this;
    let dir;
    if (arrowNeg.isSameNode(event.target)) {
      dir = -1;
    } else if ((arrowPos.isSameNode(event.target))) {
      dir = 1;
    } else {
      return;
    }
    const newState = {
      trigger: event,
      stopTriggerByElementActiveStatus: event.type === 'pointerdown',
    };
    this._startScroll(dir, newState);
  }

  _onwheel = (event) => {
    const { axis, natural } = this;
    const dir = natural ? 1 : -1;
    if (axis === XAXIS && event.deltaX !== 0) {
      event.preventDefault();
      this.moveTo(dir, event.deltaX);
    } else if (axis === YAXIS && event.deltaY !== 0) {
      event.preventDefault();
      this.moveTo(dir, event.deltaY);
    }
  }

  _startScroll = (d, newState) => {
    const { natural, initialSpeed } = this;
    let dir = d;
    const activeScrollId = this._getScrollId();
    if (natural) dir *= -1;
    this._saveState({
      ...newState,
      speed: initialSpeed,
      activeScrollId,
    });
    this._onActiveScroll(dir, activeScrollId);
  }

  stopScroll = () => {
    this._saveState({
      trigger: null,
      stopTriggerByElementActiveStatus: false,
    });
  }

  _onActiveScroll = (dir, id) => {
    const { acceleration, deceleration, state, initialSpeed } = this;
    const { speed, trigger, stopTriggerByElementActiveStatus, activeScrollId } = state;
    if (id !== activeScrollId) {
      return;
    }
    this.moveTo(dir, speed, true);
    let isActive = false;
    if (trigger && stopTriggerByElementActiveStatus) {
      isActive = trigger.target.matches(`${trigger.target.tagName}:active`);
    } else if (trigger) {
      isActive = true;
    }
    // If active acceleration else deceleration
    const delta = isActive ? acceleration : -deceleration;
    const speedCalc = Math.round(speed + (initialSpeed * delta));
    const newSpeed = speedCalc > 0 ? speedCalc : 0;
    this._saveState({
      speed: newSpeed,
    });
    if (newSpeed > 0) {
      window.requestAnimationFrame(() => this._onActiveScroll(dir, id));
    }
  }

  moveTo = (dir, step, onActiveScroll, behavior = 'auto') => {
    const { row, axis } = this;

    /*
    if moveTo is called by other method than _onActiveScroll
    the state must be reseted to interrupt any activescroll animation
    */
    if (!onActiveScroll) {
      this.resetState();
    }
    row.scrollBy({
      top: axis === YAXIS ? step * dir : 0,
      left: axis === XAXIS ? step * dir : 0,
      behavior,
    });
  }
}
