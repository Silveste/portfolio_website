import RoundedButtonHandler from '../../components/btn/script';

export default class CertificationHandler {
  constructor(
    certification,
    btnAction,
    frontBtnClass = 'js-front-btn',
    backBtnClass = 'js-back-btn',
    backSideClass = 'js-back-side',
    showBackClass = 'js-show-back',
  ) {
    const frontBtn = certification.querySelector(`.${frontBtnClass}`);
    const backBtn = certification.querySelector(`.${backBtnClass}`);
    this.certification = certification;
    this.backSide = certification.querySelector(`.${backSideClass}`);
    this.frontBtnHandler = new RoundedButtonHandler(frontBtn);
    this.backBtnHandler = new RoundedButtonHandler(backBtn);
    this.btnAction = btnAction;
    this.showBackClass = showBackClass;
  }

  init = () => {
    const { frontBtnHandler, backBtnHandler, backSide } = this;
    frontBtnHandler.init();
    backBtnHandler.init();
    backSide.addEventListener('focusin', this._showBack);
    backSide.addEventListener('focusout', this._showFront);
    this.toggleBtn();
  }

  toggleBtn = (show = true) => {
    const { frontBtnHandler, backBtnHandler } = this;
    const frontBtn = frontBtnHandler.getElement();
    const backBtn = backBtnHandler.getElement();
    frontBtnHandler.toggleBtn(show);
    backBtnHandler.toggleBtn(show);
    if (show) {
      frontBtn.ontouchend = () => this.btnAction(this);
      backBtn.ontouchend = () => this.btnAction(this);
    } else {
      frontBtn.ontouchend = null;
      backBtn.ontouchend = null;
    }
  }

  flip = () => {
    const { certification, showBackClass } = this;
    return certification.classList.toggle(showBackClass);
  }

  _showBack = () => {
    const { certification, showBackClass } = this;
    certification.classList.add(showBackClass);
  }

  _showFront = () => {
    const { certification, showBackClass } = this;
    certification.classList.remove(showBackClass);
  }
}
