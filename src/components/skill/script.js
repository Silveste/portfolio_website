import { removePxUnit, setOnInteractionUp } from '../../scripts/helpers';

// When skill is resized triggers an event
const onResize = handler => new CustomEvent('onskillresize', { bubbles: true, detail: handler });

export default class SkillHandler {
  constructor(skill) {
    this.skill = skill;
    this.skillGroups = skill.dataset.group.split(' ');
    this.description = skill.querySelector('section');
    this.img = skill.querySelector('img');
    this.btn = skill.querySelector('.js-btn');
    // ratio Width of expanded skill / width of container in function of media queries
    this.expandedWidths = [
      { mQuery: '(max-width: 480px)', ratio: 0.7 },
      { mQuery: '(max-width: 640px)', ratio: 0.7 },
      { mQuery: '(max-width: 768px)', ratio: 0.6 },
      { mQuery: '(max-width: 1024px)', ratio: 0.5 },
      { default: true, ratio: 0.5 },
    ];
    this.expandedWidth = null;
    this.shrinkedHeight = null;
    this.shrinkedWidth = null;
    this.animationPxDelta = 15;
    this.animationFPS = 50;
    this.animationTimestamp = 0;
    this.expanded = true;
    this.aFrameId = null;
  }

  getNode = () => this.skill;

  init = () => {
    const { description, btn } = this;

    // Set Expanded width
    this._setExpandedWidth();

    // Hide the description
    description.hidden = true;

    // Show button
    btn.classList.remove('js-hidden');
    setOnInteractionUp(btn, this._toggleBtn);

    // Replace static styles by dynamic
    this.skill.classList.replace('js-static-props-skill', 'js-props-skill');

    // Get shrinked sizes and shrink
    this._setShrinkedSizes();

    // Apply scale 0
    this.skill.classList.add('js-scale-0');
  }

  reset = () => {
    const { description } = this;
    // Set Expanded width
    this._setExpandedWidth();
    // Hide the description
    description.hidden = true;
    // Get shrinked sizes and shrink
    this._setShrinkedSizes();
  }

  getWidth = () => this.skill.offsetWidth;

  getHeight = () => this.skill.offsetHeight;

  setPosition = (pos) => {
    const { x, y } = pos;

    // Restart edges postion
    this.skill.style.top = null;
    this.skill.style.right = null;
    this.skill.style.bottom = null;
    this.skill.style.left = null;

    // Set new position
    this.skill.style[x.edge] = `${x.pos}px`;
    this.skill.style[y.edge] = `${y.pos}px`;
  }

  move = (x = 0, y = 0) => {
    const { skill } = this;

    // Get position Styles
    const compStyle = window.getComputedStyle(skill, null);
    const keys = ['top', 'right', 'bottom', 'left'];
    const posStyles = keys.reduce((acc, val) => {
      const position = skill.style[val];
      if (position) {
        acc.push({
          edge: (val === 'top' || val === 'bottom') ? 'y' : 'x',
          // If edges are bottom and/or right the move is in the oposite direction
          dir: (val === 'top' || val === 'left') ? 1 : -1,
          key: val,
          value: removePxUnit(compStyle.getPropertyValue(val)),
        });
      }
      return acc;
    }, []);
    const posH = posStyles.find(val => val.edge === 'y');
    const posW = posStyles.find(val => val.edge === 'x');
    skill.style[posW.key] = `${posW.value + (x * posW.dir)}px`;
    skill.style[posH.key] = `${posH.value + (y * posH.dir)}px`;
  }

  popup = () => this.skill.classList.replace('js-scale-0', 'js-scale-100');
  popdown = () => this.skill.classList.replace('js-scale-100', 'js-scale-0');
  isGroupPresent = (group) => {
    const { skillGroups } = this;
    const result = skillGroups.indexOf(group);
    return result >= 0;
  }

  _setShrinkedSizes = () => {
    const { skill, img } = this;

    // Get area % from dataset & img
    const areaPctg = parseInt(skill.dataset.area, 10);
    if (!areaPctg) {
      return;
    }

    // Get parent size and calcualate area in px
    const parentW = skill.parentElement.clientWidth;
    const parentH = skill.parentElement.clientHeight;
    const parentArea = parentW * parentH;

    // Get skill area in px
    const area = (areaPctg / 100) * parentArea;

    // Get img aspect ratio (W/H)
    const natHeight = img.naturalHeight;
    const natWidth = img.naturalWidth;
    const ratio = natWidth / natHeight;

    // Get skill padding
    const compStyle = window.getComputedStyle(skill, null);
    const pb = removePxUnit(compStyle.getPropertyValue('padding-bottom'));
    const pt = removePxUnit(compStyle.getPropertyValue('padding-top'));
    const pl = removePxUnit(compStyle.getPropertyValue('padding-left'));
    const pr = removePxUnit(compStyle.getPropertyValue('padding-right'));

    // Set dimensions
    const imgWidth = Math.round(Math.sqrt(area * ratio));
    const imgHeight = Math.round(Math.sqrt(area / ratio));
    const newWidth = pl + pr + imgWidth;
    const newHeight = pt + pb + imgHeight;
    this.shrinkedWidth = newWidth;
    this.shrinkedHeight = newHeight;
    skill.style.width = `${newWidth}px`;
    skill.style.height = `${newHeight}px`;
    img.style.width = `${imgWidth}px`;
    img.style.height = `${imgHeight}px`;
    this.expanded = false;
  }

  _toggleBtn = () => {
    const { expanded, aFrameId } = this;
    // If animation is running toggleBtn doesn work
    if (aFrameId) return;
    if (expanded) {
      this.shrink();
    } else {
      this.expand();
    }
  }

  expand = () => {
    const { expanded, btn, aFrameId } = this;
    const arrow = btn.querySelector('.js-btn-content');
    let expansible = !expanded;
    if (aFrameId) {
      window.cancelAnimationFrame(aFrameId);
      this.aFrameId = null;
      this.expanded = false;
      expansible = true;
    }
    if (expansible) {
      this.expanded = false;
      arrow.classList.replace('js-rotate-90', 'js-rotate-270');
      this.description.hidden = false;
      this.aFrameId = window.requestAnimationFrame(newTimestamp => this._animateExpansion(newTimestamp));
    }
  }

  shrink = () => {
    const { expanded, btn, aFrameId } = this;
    const arrow = btn.querySelector('.js-btn-content');
    let shrinkable = expanded;
    if (aFrameId) {
      window.cancelAnimationFrame(aFrameId);
      this.aFrameId = null;
      this.expanded = true;
      shrinkable = true;
    }
    if (shrinkable) {
      // NOTE: Unlike expand method classes are toggled
      // at the end of animation
      arrow.classList.replace('js-rotate-270', 'js-rotate-90');
      this.aFrameId = window.requestAnimationFrame(newTimestamp => this._animateExpansion(newTimestamp));
    }
  }

  _animateExpansion = (timestamp) => {
    const { expanded, animationFPS, animationTimestamp } = this;
    const nextStepTime = animationFPS / 1000;

    if (timestamp - nextStepTime >= animationTimestamp) {
      this.animationTimestamp = timestamp;
      const isFinished = this._boxResize();
      this.skill.dispatchEvent(onResize(this));
      if (isFinished) {
        this.expanded = !expanded;
        this.aFrameId = null;
        return;
      }
    }
    this.aFrameId = window.requestAnimationFrame(newTimestamp => this._animateExpansion(newTimestamp));
  }

  _boxResize = () => {
    const { expanded } = this;
    // if expanded resize first height and then width, else the oposite
    let isFinished = false;
    if (expanded) {
      const isHUpdated = this._setNewHeight();
      const isWUpdated = isHUpdated ? this._setNewWidth() : false;
      isFinished = isWUpdated;
    } else {
      const isWUpdated = this._setNewWidth();
      const isHUpdated = isWUpdated ? this._setNewHeight() : false;
      isFinished = isHUpdated;
    }
    return isFinished;
  };

  _setNewWidth = () => {
    const {
      animationPxDelta,
      expanded,
      expandedWidth,
      shrinkedWidth,
      skill,
    } = this;

    const { offsetWidth } = skill;

    // Some skills may have longer width than expanded width. In that case width remains constant
    if (offsetWidth > expandedWidth) return true;

    let newWidth;
    if (expanded) {
      const nextWidth = offsetWidth - animationPxDelta;
      newWidth = nextWidth > shrinkedWidth ? nextWidth : shrinkedWidth;
    } else {
      const nextWidth = offsetWidth + animationPxDelta;
      newWidth = nextWidth < expandedWidth ? nextWidth : expandedWidth;
    }

    if (newWidth !== offsetWidth) {
      skill.style.width = `${newWidth}px`;
      return false;
    }
    // If newwidth is === offsetWidth return true
    return true;
  }

  _setNewHeight = () => {
    const {
      animationPxDelta,
      expanded,
      shrinkedHeight,
      skill,
    } = this;

    const { offsetHeight, scrollHeight } = skill;

    let newHeight;
    if (expanded) {
      const nextHeight = offsetHeight - animationPxDelta;
      newHeight = nextHeight > shrinkedHeight ? nextHeight : shrinkedHeight;
    } else {
      const nextHeight = offsetHeight + animationPxDelta;
      newHeight = nextHeight < scrollHeight ? nextHeight : scrollHeight;
    }

    if (newHeight !== offsetHeight) {
      skill.style.height = `${newHeight}px`;
      return false;
    }
    // If newHeight is === offsetHeight return true
    return true;
  }

  _setExpandedWidth = () => {
    const { expandedWidths, skill } = this;
    const match = expandedWidths.find(
      val => val.default || window.matchMedia(val.mQuery).matches,
    );
    const parentWidth = skill.parentElement.clientWidth;
    this.expandedWidth = Math.round(parentWidth * match.ratio);
  }
}
