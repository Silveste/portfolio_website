export default class RoundedButtonHandler {
  constructor(button) {
    this.button = button;
    this.state = {
      active: true,
    };
  }

  init = () => {
    const { state } = this;
    const { active } = state;
    this.toggleBtn(active);
  }

  getElement = () => this.button;

  toggleBtn = (show = true) => {
    const { button } = this;
    if (show) {
      button.classList.remove('js-hidden');
    } else {
      button.classList.add('js-hidden');
    }
  }
}
