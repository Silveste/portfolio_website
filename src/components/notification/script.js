import { applyAfterTransparent, setOnInteractionUp } from '../../scripts/helpers';

class MessageHandler {
  constructor(
    container = document.getElementById('notification').content.cloneNode(true),
    titleClass = 'js-title',
    contentClass = 'js-content',
    closeHandlerClass = 'js-close',
    initClass = 'js-lc-notification-initial',
    finalClass = 'js-lc-notification-final',
    closeClass = 'js-lc-notification-remove',
    renderingTime = 7500,
  ) {
    this.container = container;
    this.titleClass = titleClass;
    this.contentClass = contentClass;
    this.closeHandlerClass = closeHandlerClass;
    this.initClass = initClass;
    this.finalClass = finalClass;
    this.closeClass = closeClass;
    this.renderingTime = renderingTime;
    this.messages = [];
    this.state = {
      renderingMsjId: null,
      renderingContainer: null,
    };
  }

  sendMsg = (title, message) => {
    const id = Math.round(Math.random() * 1000000);
    const paragraphs = [].concat(message);
    this.messages.push({ id, title, paragraphs });
    this._render();
    return id;
  }

  removeMsg = (id) => {
    const { messages } = this;
    const newMessages = messages.filter(val => val.id !== id);
    if (messages.length > newMessages.length) {
      this.messages = newMessages;
      return true;
    }
    return false;
  }

  close = () => {
    const {
      state,
      finalClass,
      closeClass,
      closeHandlerClass,
    } = this;
    const {
      renderingMsjId,
      renderingContainer,
    } = state;
    const closeHandler = renderingContainer.querySelector(`.${closeHandlerClass}`);
    // Remove btn event listeners
    setOnInteractionUp(closeHandler);
    window.clearTimeout(renderingMsjId);
    renderingContainer.classList.replace(finalClass, closeClass);
    applyAfterTransparent(renderingContainer, null, false, true);
    this.state = {
      renderingMsjId: null,
      renderingContainer: null,
    };
    this._render();
  }

  _render = () => {
    const {
      state,
      messages,
      container,
      titleClass,
      contentClass,
      closeHandlerClass,
      initClass,
      finalClass,
      renderingTime,
    } = this;
    // _render could be called when a msg is being rendered or when there is no messages to render
    if (state.renderingMsjId !== null || messages.length === 0) return;

    // Append message to the body
    const message = messages.shift();
    const newContainer = container.cloneNode(true).querySelector('figure');
    const msgTitleElement = newContainer.querySelector(`.${titleClass}`);
    const msgContentElement = newContainer.querySelector(`.${contentClass}`);
    const closeHandler = newContainer.querySelector(`.${closeHandlerClass}`);
    setOnInteractionUp(closeHandler, this.close);
    msgTitleElement.textContent = message.title;
    message.paragraphs.forEach((val) => {
      const para = document.createElement('p');
      para.textContent = val;
      msgContentElement.appendChild(para);
    });
    // msgContentElement.textContent = message.message;
    document.body.appendChild(newContainer);

    // Update state and execute transitions (See CSS)
    window.setTimeout(() => newContainer.classList.replace(initClass, finalClass), 100);
    const renderingMsjId = window.setTimeout(this.close, renderingTime + 100);
    this.state = {
      renderingMsjId,
      renderingContainer: newContainer,
    };
  }
}

export default new MessageHandler();
