import { getInterval } from './helpers';
import { directions } from './enums';

class Observer {
  constructor(
    rootElement = null,
    intersectFrom = 0,
    intersectTo = 1,
    intersectPrecision = 10,
  ) {
    const options = {
      root: rootElement,
      rootMargin: '0px',
      threshold: getInterval(intersectFrom, intersectTo, intersectPrecision),
    };
    this.observer = new IntersectionObserver(this._runCallbacks, options);


    /*
    observables: Map that uses DOMElement as key and object with the following properties as a value:
      Mapkey: DOM element that is being observed
      {
        callbacks: Set of functions to call when observed is being intersected with rootElement
        state: Object with Inf required to find out the intersection direction
          preX: previous entry boundingclientRect.x
          preY: previous entry boundingclientRect.y
      }
    */
    this.observables = new Map();
  }

  _runCallbacks = (entries, observer) => {
    const { observables } = this;
    entries.forEach((entry) => {
      const observable = observables.get(entry.target);
      const currentX = entry.boundingClientRect.x;
      const currentY = entry.boundingClientRect.y;
      let { preX, preY } = observable.state;
      const dir = {
        x: null,
        y: null,
      };
      if (preX === null) {
        preX = currentX;
      }
      if (preY === null) {
        preY = currentY;
      }
      // if pre === current direction is null
      if (preX < currentX) {
        dir.x = directions.RIGHT;
      } else if (preX > currentX) {
        dir.x = directions.LEFT;
      }
      if (preY < currentY) {
        dir.y = directions.DOWN;
      } else if (preY > currentY) {
        dir.y = directions.UP;
      }
      observable.state = {
        preX: currentX,
        preY: currentY,
      };
      observable.callbacks.forEach((cb) => {
        cb(entry, dir, observer);
      });
    });
  }

  addObservable = (observed, callback) => {
    const { observables, observer } = this;
    const observable = observables.get(observed);
    let callbacks;
    let state;
    if (observable) {
      ({ callbacks, state } = observable);
      callbacks.add(callback);
    } else {
      observer.observe(observed);
      callbacks = new Set();
      callbacks.add(callback);
      state = { preX: null, preY: null };
    }
    this.observables.set(observed, { callbacks, state });
  }

  removeObservable = (observed, callback) => {
    const { observables, observer } = this;
    if (!observables.has(observed)) return;
    const { callbacks } = observables.get(observed);
    callbacks.delete(callback);
    if (callbacks.size === 0) {
      observer.unobserve(observed);
      observables.delete(observed);
    }
  };
}

export default new Observer();
