export const directions = {
  UP: 'upDirection',
  DOWN: 'downDirection',
  LEFT: 'leftDirection',
  RIGHT: 'rightDirection',
  ALLDIR: 'allDirection',
  NODIR: 'noneDirection',
};

export const axes = {
  XAXIS: 'x',
  YAXIS: 'y',
  ZAXIS: 'z',
};

export const sides = {
  RIGHT: 'right',
  LEFT: 'left',
  TOP: 'top',
  BOTTOM: 'bottom',
};
