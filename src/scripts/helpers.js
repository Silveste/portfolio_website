export const applyAfterTransparent = (element, classToApply, removeClass = false, removeElement = false) => {
  const opacity = window.getComputedStyle(element).getPropertyValue('opacity');
  if (opacity > 0) {
    window.requestAnimationFrame(() => applyAfterTransparent(element, classToApply));
  } else {
    if (removeElement) {
      // element.remove not supported by IE and possible by Opera
      element.parentNode().removeChild(element);
      return;
    }
    const cls = element.classList;
    if (removeClass) {
      cls.remove(classToApply);
    } else {
      cls.add(classToApply);
    }
  }
};

export const getInterval = (from, to, steps) => {
  const interval = (to - from) / (steps - 1);
  const result = [];
  for (let i = 0; i < steps; i++) {
    result.push(from + (i * interval));
  }
  return result;
};

export const getTarget = (event, className) => {
  let path;
  if (event.composedPath) {
    path = event.composedPath();
  } else {
    path = [event.target];
  }
  const target = path.find((val) => {
    if (val.classList) {
      return val.classList.contains(className);
    }
    return false;
  });
  return target;
};

export const setPointerNoTouch = (event, callback) => {
  if (event.pointerType === 'touch') return;
  callback(event);
};

// Set a callback for pointerup, keyup (enter) and touchend events
export const setOnInteractionUp = (element, callback) => {
  // To remove event listeners callback might be undefined or null
  if (!callback) {
    element.onpointerup = null;
    element.onkeyup = null;
    element.ontouchend = null;
    return;
  }
  element.onpointerup = (e) => {
    if (e.pointerType === 'touch') return;
    callback(event);
  };
  element.onkeyup = (e) => {
    if (e.key === 'Enter') callback(e);
  };
  element.ontouchend = e => callback(e);
};

export const quadraticEaseOut = (x, minY, maxY, maxX) => {
  const xRatio = x / maxX;
  return ((-maxY * xRatio) * (xRatio - 2)) + minY;
};

export const quadraticEaseIn = (x, minY, maxY, maxX) => {
  const xRatio = x / maxX;
  return (maxY * (xRatio ** 4)) + minY;
};

export const quadraticEaseInOutInverse = (x, minY, maxY, maxX) => {
  let xRatio = x / (maxX / 2);
  if (xRatio < 1) return ((-(maxY / 2) * xRatio) * (xRatio - 2)) + minY;
  xRatio -= 1;
  return ((maxY / 2) * ((xRatio) ** 4)) + (minY + (maxY / 2));
};

export const removePxUnit = string => parseInt(string.slice(0, -2), 10);

/*
Parse a string and return and "a" html tag with the attributes specified in attributesString whenever the string have the following pattern:
[<a tag inner text>]("<url>","<title>")
*/
export const parseLinks = (string, attributesString) => {
  const mdLinkRegex = /\[([\s\S]+?)\]\(("|')(https?:\/\/\S+?)\2,("|')([\s\S]+?)\4\)/g;
  return string.replace(
    mdLinkRegex,
    `<a href="$3" ${attributesString} title="$5">$1</a>`,
  );
};

/*
* Serialize form data into query string
* @param {Node} form to serialize
* @return {String} serialized data
*/
export const serialize = (form) => {
  const nodes = [...form.children];
  // input should have: name and not be disbled and not be a button nor submit nor reset not file type
  const nodesToSerialize = nodes.filter(node => node.name && !node.disabled && !(node.type === 'submit' || node.type === 'button' || node.type === 'file' || node.type === 'reset' || node.type === 'image'));
  return nodesToSerialize.reduce((acc, val, index) => {
    let str = acc;
    if (index !== 0) str += '&';
    str += `${encodeURIComponent(val.name)}=${encodeURIComponent(val.value)}`;
    return str;
  }, '');
};

/*
* watchElement returns a promise that is resolved when a certain DOM element key meet the
* value indicated. Or is rejected after certain execution time (5000 ms by default)
* or the contion set by a function.
* @Params:
* @Params:
*   - element: DOM element on which some future changes are expected
*   - resolveIf: function that receives the element as a parameter and returns a
*     boolean. If the returning value is true the promise will be fullfilled.
*   - (Optional) rejectIf: parameter that accepts 2 different types:
*         - Number: Indicates the max number in miliseconds that the promise will wait
*           for fullfillment. After that the promise is rejected
*         - callback function: Receives 2 parameters:
*                element: DOM element passed to the promise
*                executionTime: Time in miliseconds since the promise is pending.
*                The callback should return a boolean value. If the value is true the
*                promise is rejected
* @Return Promise. The DOM element passed as argument is returned by both resolve
* and reject smethods (can be used in the then or catch methods)
*/
export const watchElement = (element, resolveIf, rejectIf = 5000) => new Promise((resolve, reject) => {
  let initialTime;
  // Set recursive function to be called with requestAnimationFrame
  const runner = (timeStamp) => {
    // Set intial time
    if (!initialTime) {
      initialTime = timeStamp;
    }
    // Check if should be rejected
    let shouldReject;
    switch (typeof rejectIf) {
    case 'number':
      shouldReject = timeStamp - initialTime > rejectIf;
      break;
    case 'function':
      shouldReject = rejectIf(element, timeStamp - initialTime);
      break;
    default:
      throw new Error('Invalid type of parameter rejectIf');
    }
    if (shouldReject) return reject(element);
    // Check if should be approved
    if (resolveIf(element)) return resolve(element);
    // If is not rejected or approved call the runner again
    return window.requestAnimationFrame(runner);
  };
  window.requestAnimationFrame(runner);
});
