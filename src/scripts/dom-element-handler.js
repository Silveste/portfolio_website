// TODO Create dom-element-handler-interface to make all handlers inherit from it
/*
The Handler class provides a wrapper for DOM elements.
the aim is either to provide a clean code to get/set typical properties of the DOM Node or to provide new functionality.

The handler must have 3 mothods:
 * .getWidth() => returns the width of the child element
 * .getHeight() => returns the heiht of the child element
 * .setPosition => method to set the postion, requires an object with x and y position
*/
