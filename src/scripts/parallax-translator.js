import rootObserver from './observers';
import { axes } from './enums';

const { XAXIS } = axes;

export default class ParallaxAnimation {
  /*
  observed: DOM element
  The intersection ratio between viewport and observed element is used
  to calculate the translation of animatedElements
  */
  constructor(refElement) {
    this.refElement = refElement;

    /*
      animatedElements is an array of objects with
      inf about elements to translate and how to translate
        axis: Values are X, Y
        boost: How much the element will be moved valus from 0 to infinite
        reverse: boolean, if true direction is negative,
        element: DOM element to translate
    */
    this.animatedElements = null;
    this.animationFrameId = null;
  }

  init = () => {
    const { refElement } = this;
    rootObserver.addObservable(refElement, this._toggleAnimation);
  }

  start = this.init;

  stop = () => {
    const { refElement, animationFrameId } = this;
    rootObserver.removeObservable(refElement, this._toggleAnimation);
    this.animatedElements.forEach(({ element }) => {
      element.classList.remove('js-will-change-transform');
      element.style.transform = '';
    });
    window.cancelAnimationFrame(animationFrameId);
    this.animationFrameId = null;
  }

  animateElement = (element, axis, boost, reverse = false) => {
    if (!this.animatedElements) {
      this.animatedElements = [];
    }
    this.animatedElements.push({
      element,
      axis,
      boost,
      reverse,
    });
  }

  _toggleAnimation = (entry) => {
    const { animationFrameId } = this;
    if (!animationFrameId && entry.isIntersecting) {
      this.animatedElements.forEach(({ element }) => element.classList.add('js-will-change-transform'));
      this.animationFrameId = window.requestAnimationFrame(this._activateParallax);
    } else if (animationFrameId && !entry.isIntersecting) {
      this.animatedElements.forEach(({ element }) => element.classList.remove('js-will-change-transform'));
      window.cancelAnimationFrame(animationFrameId);
      this.animationFrameId = null;
    }
  }

  _activateParallax = () => {
    const { refElement } = this;
    const factor = refElement.getBoundingClientRect().top * -1;
    this.animatedElements.forEach((val) => {
      // NOTE: The transforms applied here will override any transform included in the CSS
      const { element, axis, boost, reverse } = val;
      let translateValue = Math.round(boost * factor);
      if (reverse) {
        translateValue *= -1;
      }
      let translateString;
      if (axis === XAXIS) {
        translateString = `translate3d(${translateValue}px,0,0)`;
      } else {
        translateString = `translate3d(0, ${translateValue}px,0)`;
      }
      // Check if translate3d css function is already in element.style.transform
      const regex = /translate3d\(.+?,.+?,.+?\)/gi;
      if (regex.test(element.style.transform)) {
        element.style.transform = element.style.transform.replace(regex, translateString);
      } else {
        element.style.transform += ` ${translateString}`;
      }
    });
    this.animationFrameId = window.requestAnimationFrame(this._activateParallax);
  }
}
