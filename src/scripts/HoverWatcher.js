class HoverWatcher {
  constructor(canHoverClass) {
    this.canHoverClass = canHoverClass;
    this.state = { canHover: false };
    this.callbacks = [];
  }

  init = () => {
    document.addEventListener('touchstart', this._removeCanHover);
    document.addEventListener('pointerover', this._addCanHover);
  }

  addCallback = (fcn) => {
    const { callbacks } = this;
    callbacks.push(fcn);
  }

  _addCanHover = (e) => {
    const { state, canHoverClass } = this;
    if (e.pointerType !== 'touch' && !state.canHover) {
      document.documentElement.classList.add(canHoverClass);
      this.state.canHover = true;
      this._execCallbacks(e.pointerType);
    }
  }

  _removeCanHover = () => {
    const { state, canHoverClass } = this;
    if (state.canHover) {
      document.documentElement.classList.remove(canHoverClass);
      this.state.canHover = false;
      this._execCallbacks('touch');
    }
  }

  _execCallbacks = (pointerType) => {
    const { callbacks } = this;
    callbacks.forEach(fcn => fcn(pointerType));
  }
}

const hoverWatcher = new HoverWatcher('js-can-hover');
export default hoverWatcher;
