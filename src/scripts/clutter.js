/*
Clutter takes the children of a container and spread them in a random way along all the container.

Note that the container must have a defined width an height

constructor arguments:
  - container: DOM element that contains the elements to be shuffled
  - childHandlers: Array of Handlers for DOM child elements. (See Handler class)
*/
import { sides } from './enums';
const { TOP, BOTTOM, RIGHT, LEFT } = sides;

export default class Clutter {
  constructor(container, childHandlers) {
    this.container = container;
    this.childHandlers = childHandlers;
    this.containerWidth = container.clientWidth;
    this.containerHeight = container.clientHeight;
  }

  clutter = () => {
    const { childHandlers, containerWidth, containerHeight } = this;
    childHandlers.forEach((handler) => {
      // get child width and height + estimated for shadows, margins etc
      const estimated = 1.1;
      const childW = handler.getWidth() * estimated;
      const childH = handler.getHeight() * estimated;

      // Get Random child box edges where position applies
      const edgeX = this._getRandom(true) ? LEFT : RIGHT;
      const edgeY = this._getRandom(true) ? TOP : BOTTOM;

      // get X and Y random positions
      const posX = this._getRandom(true, 0, containerWidth - childW);
      const posY = this._getRandom(true, 0, containerHeight - childH);
      // Set child position
      handler.setPosition({
        x: {
          edge: edgeX,
          pos: posX,
        },
        y: {
          edge: edgeY,
          pos: posY,
        },
      });
    });
  }

  _getRandom = (integer = false, min = 0, max = 1) => {
    const number = min + (Math.random() * (max - min));
    return integer ? Math.round(number) : number;
  };
}
