/*
This class provides user typing effect to the text that is in the body.
It is SEO friendly as works by collecting the dom elements inside a container.

The TypeWritter instance removes the child elements of the container when call the init() method and parse them.

Depending on the data in the markup, the TypeWritter instance can print different lines at once or imitating user typing

Markup structure Requirements:
- Container: The DOM eleemnt is passed to the constructor.
- Container children shouldn't have IDs (as it might be duplicated)
- Lines: Block element (the code will set as a display: block), which contain one or more children elements with the line text.
  Attributes:
    data-start (optional) indicates the delay in ms before the line has been printed.
    data-end (optional) indicates the delay in ms after the line has been printed.
- Text wrapper: inline-block (Will be set as display: inline-block) Text wrapper element that doesn contain children nodes only text
  Attributes:
    data-start: (optional) indicates the delay in ms before the text has been printed.
    data-end: (optional) indicates the delay in ms after the text has been printed.
    data-duration: (optional) indicates the duration of typing (if not present the whole text will be printed at once)

How it works:
  When instantiated, type writter reads the markup under the cotnainer and parse it storing all the information that it needs in an object.
  When init is executed, type writter replace the markup and insert new markup with the inline style color set to transparent.
  The new markup have new inline wrappers, one per caracter that is printed independenly.
  When the animation is running the inline style is removed to simulate character printing
  Note that the display property of lines can be set to none at runtime. therefore new lines can be shown or hidden
Properties:
  cursorColor: Indicates the color of the cursor
  cursorType: 3 types: block, vertical or underscore
*/
const STEP = {
  startLine: 'startLine',
  endLine: 'endLine',
  startNode: 'startNode',
  endNode: 'endNode',
};

export default class TypeWritter {
  constructor(
    container,
    infinite = false,
    cursorClass,
    lineClass = 'js-line',
    textClass = 'js-text',
  ) {
    this.lineClass = lineClass;
    this.textClass = textClass;
    this.infinite = infinite;
    this.container = container;
    this.cursorClass = cursorClass;
    this.hideStyle = {
      prop: 'color',
      val: 'transparent',
    };
    this.callback = null;
    /*
      this.data structure: (see _setData method)
      Array of objects:
      [{
        lineEl: dom element that represents a line,
        timeStart: Delay before the line has been printed
        timeEnd: Delay after the line has been printed
        wrappersData: Single linked list of objects with information for every wrapper in the line:
          {
            wrappEl: Dom element that wrapps the text
            timeStart: Delay before the wrapper is printed
            timeEnd: Delay after the wrapper is printed
            nextNode: Next linked list node
            cursor: indicates if node is a wrapper for the cursor
          }
      }]
    */
    this.data = null;
    this.state = {
      nextStep: STEP.startLine,
      nextActionTime: null,
      lineIndex: 0,
      textWrapper: false,
    };
    this.animationFrameId = null;
  }

  init = (start = true) => {
    if (!this.data) {
      const raw = this._getStaticHTML();
      this._setData(raw);
    }
    if (start) this.start();
  }

  start = () => {
    const { animationFrameId } = this;
    if (!animationFrameId) {
      this.animationFrameId = window.requestAnimationFrame(this._runAnimation);
    }
  }

  stop = (reset = true) => {
    const { animationFrameId } = this;
    if (animationFrameId === null) return;
    window.cancelAnimationFrame(animationFrameId);
    this.animationFrameId = null;
    if (reset) this._reset();
  }

  setCallback = (callback) => {
    this.callback = callback;
  }

  isAnimationOff = () => {
    const { animationFrameId } = this;
    return animationFrameId === null;
  }

  _getStaticHTML = () => {
    const { container } = this;
    return Array.from(container.children);
  }

  _setData = (rawHTML) => {
    const { lineClass } = this;
    // Using concat allows to pass unique value or array
    const lines = [].concat(rawHTML);
    lines.forEach((line) => {
      // Only lines with the line selector are considered
      if (!line.classList.contains(lineClass)) return;
      const children = Array.from(line.children);
      // If line has no children return
      if (!children) return;
      const timeEnd = line.dataset.end ? parseInt(line.dataset.end, 10) : 0;
      const timeStart = line.dataset.start ? parseInt(line.dataset.start, 10) : 0;
      const lineEl = line;
      // Update DOM with new html and store data
      const wrappersData = this._setNewHtml(children, false);
      if (!this.data) this.data = [];
      this.data.push({ timeStart, timeEnd, lineEl, wrappersData });
    });
  }

  /*
    Store the data in a single linked list,
    Note that if children has more children the method call itself recursively
    @Param children: Array of sibbling DOM elements
    @Param acumulator: Linked list node of wrapperData (See coment above).
           note that last node must be false so first call to the method accumulator == false
    @return: Data linked list
    Note that the method modify the DOM adding new span elements that wraps the text contained in elements with the class textClass
  */
  _setNewHtml = (children, acumulator) => children.reduceRight((acc, el) => {
    const { textClass, cursorClass } = this;
    // If element is a cursor hide it and return cursor node
    if (el.classList.contains(cursorClass)) return this._setCursorNode(el, acc);

    // Elements with children will loop through children first
    const list = el.children.length ? this._setNewHtml(Array.from(el.children), acc) : acc;

    // if element doesn't have textClass return list
    if (!el.classList.contains(textClass)) return list;

    // Get Eelement dataset params and text content
    const duration = el.dataset.duration ? parseInt(el.dataset.duration, 10) : 0;
    const timeEnd = el.dataset.end ? parseInt(el.dataset.end, 10) : 0;
    const timeStart = el.dataset.start ? parseInt(el.dataset.start, 10) : 0;
    const text = el.textContent;

    // If duration is 0 return new node
    if (duration === 0) return this._createWrapperNode(el, timeStart, timeEnd, text, list);

    // For duration > 0 get delay times for each character
    const times = this._getWaitingTimes(text, duration);

    // loop over the times and set a new wrapper for each character
    let nextNode = list;
    times.forEach((time, index) => {
      // time is applied as starttime: delay before the character is printed
      // Elements will be created in reverse order
      // Therefore starttime is applied at the end and endtime at the beginning
      const newTimeStart = index + 1 === times.length ? timeStart + time : time;
      const newTimeEnd = index === 0 ? timeEnd : 0;
      // Get the chars in reverse order
      const char = text.charAt(text.length - (index + 1));

      // Create new node
      nextNode = this._createWrapperNode(el, newTimeStart, newTimeEnd, char, nextNode);
    });
    return nextNode;
  }, acumulator);

  _setCursorNode = (cursorEl, acc) => {
    const { cursorClass } = this;
    cursorEl.classList.remove(cursorClass);
    return { wrappEl: cursorEl, timeStart: 0, timeEnd: 0, nextNode: acc, cursor: true };
  }

  _createWrapperNode = (parent, timeStart, timeEnd, text, nextNode) => {
    const { hideStyle } = this;

    // Create the DOM element
    const wrappEl = document.createElement('span');
    wrappEl.dataset.start = timeStart;
    wrappEl.dataset.end = timeEnd;
    wrappEl.textContent = text;
    wrappEl.style[hideStyle.prop] = hideStyle.val;

    //  Apply to the parent
    if (parent.firstChild.nodeType === Node.TEXT_NODE) {
      parent.replaceChild(wrappEl, parent.firstChild);
    } else {
      parent.insertBefore(wrappEl, parent.firstChild);
    }

    // Return the linked list
    return { wrappEl, timeStart, timeEnd, nextNode, cursor: false };
  }

  _runAnimation = (timeStamp) => {
    const { data, state, hideStyle, infinite, callback, cursorClass } = this;
    const { nextActionTime, nextStep, lineIndex, textWrapper } = state;
    const line = data[lineIndex];
    const isLastLine = data.length === lineIndex + 1;
    if (textWrapper && cursorClass) textWrapper.wrappEl.classList.add(cursorClass);
    // Line display status can be changed at runtime by changing display property
    // if display is none the delay is zero
    let skipAnimationFrame = false;
    if (window.getComputedStyle(line.lineEl).display === 'none') {
      skipAnimationFrame = true;
    }
    if (timeStamp >= nextActionTime || skipAnimationFrame) {
      switch (nextStep) {
      case STEP.startLine:
        this._updateState({
          nextStep: STEP.startNode,
          textWrapper: line.wrappersData,
          nextActionTime: timeStamp + line.timeStart,
        });
        break;
      case STEP.startNode:
        this._updateState({
          nextStep: STEP.endNode,
          nextActionTime: timeStamp + textWrapper.timeStart,
        });
        break;
      case STEP.endNode:
        if (!textWrapper.cursor) textWrapper.wrappEl.style[hideStyle.prop] = null;
        if (cursorClass && !textWrapper.cursor) textWrapper.wrappEl.classList.remove(cursorClass);
        this._updateState({
          nextStep: textWrapper.nextNode ? STEP.startNode : STEP.endLine,
          textWrapper: textWrapper.nextNode,
          nextActionTime: timeStamp + textWrapper.timeEnd,
        });
        break;
      case STEP.endLine:
        this._updateState({
          nextStep: isLastLine ? null : STEP.startLine,
          lineIndex: isLastLine ? 0 : lineIndex + 1,
          nextActionTime: timeStamp + line.timeEnd,
        });
        break;
      // default call callback and stop if not infinite
      default:
        if (callback) this.callback();
        if (!infinite) {
          this.animationFrameId = null;
          return;
        }
        this._reset();
      }
    }
    if (skipAnimationFrame) {
      this._runAnimation(timeStamp);
    } else {
      this.animationFrameId = window.requestAnimationFrame(this._runAnimation);
    }
  }

  _getWaitingTimes = (content, delay) => {
    // Get an array of text lenght with random numbers that sums delay (approx)
    const charsNumber = content.length;
    const avgDelay = delay / charsNumber;
    // Delta must be between 0.5 or 0
    const delta = 0.5;
    const rndmDelays = [];
    let deviation = 0;
    for (let i = 0; i < charsNumber; i++) {
      // Get rndom number between -delta and +delta
      const rndmDelta = (Math.random() * 2 * delta) - delta;

      // Get random number from avgDelay with max variation = -/+ delta
      // deviation is used to track deviation from avg and added in each step
      // to minimise the overall deviation.
      // Last step only updates avg + deviation to achieve the correct delay
      let rndmAvg = 0;
      if (i < charsNumber - 1) {
        rndmAvg = Math.round(deviation + avgDelay + ((deviation + avgDelay) * rndmDelta));
      } else {
        rndmAvg = Math.round(deviation + avgDelay);
      }

      // push into the array
      rndmDelays.push(rndmAvg);

      // Update deviation
      deviation += avgDelay - rndmAvg;
    }
    return rndmDelays;
  }

  _reset = () => {
    const { data, hideStyle, container, cursorClass } = this;
    if (cursorClass) container.querySelector(`.${cursorClass}`).classList.remove(cursorClass);
    data.forEach((val) => {
      let nextWrapper = val.wrappersData;
      while (nextWrapper) {
        nextWrapper.wrappEl.style[hideStyle.prop] = hideStyle.val;
        nextWrapper = nextWrapper.nextNode;
      }
    });
    this._updateState({
      nextActionTime: null,
      lineIndex: 0,
      textWrapper: false,
      nextStep: STEP.startLine,
    });
  }

 _updateState = (keys) => {
   const { state } = this;
   this.state = {
     ...state,
     ...keys,
   };
 }
}
