const merge = require('webpack-merge');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetsWebpackPlugin = require('optimize-css-assets-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');
const path = require('path');
const common = require('./webpack.common');

const prod = {
  mode: 'production',
  plugins: [
    new CleanWebpackPlugin(),
    new MiniCssExtractPlugin({filename: 'style-[contenthash:4]-bundle.css'}),
    new CopyPlugin([{ from: 'src/conf', to: './' }]),
    new FaviconsWebpackPlugin({
      logo: path.resolve(__dirname, '../src/assets/imgs/fav-icon-v2.svg'),
      prefix: 'assets/favicons/',
      mode: 'light', // optional can be 'webapp' or 'light' - 'webapp' by default
      favicons: {
        background: '#f5f5ef',
        theme_color: '#45a084'
      }
    })
  ],
  optimization: {
    minimizer: [
      new OptimizeCssAssetsWebpackPlugin(),
      new TerserPlugin()
    ]
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
        ]
      },
      {
        test: /\.(png|jpe?g|gif)$/,
        use: {
          loader: 'responsive-loader',
          options: {
            name: '[name]-[width]-[contenthash:4].[ext]',
            outputPath: 'assets/imgs'
          }
        },
        include: path.resolve(__dirname,'../src/assets/imgs')
      }
    ]
  },
  output: {
    filename: 'main-[contenthash:4]-bundle.js',
    path: path.resolve(__dirname,'../dist'),
    publicPath: '/'
  }
};

module.exports = merge.smartStrategy({'module.rules.use': 'prepend'})(common,prod);
