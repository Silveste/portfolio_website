const merge = require('webpack-merge');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const path = require('path');
const common = require('./webpack.common');

const test = {
  mode: 'production',
  plugins: [
    new CleanWebpackPlugin(),
    new MiniCssExtractPlugin({filename: 'style-[contenthash:4]-bundle.css'})
  ],
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
        ]
      },
      {
        test: /\.(png|jpe?g|gif)$/,
        use: {
          loader: 'responsive-loader',
          options: {
            name: '[name]-[width]-[contenthash:4].[ext]',
            outputPath: 'assets/imgs',
            disable: true
          }
        },
        include: path.resolve(__dirname,'../src/assets/imgs')
      }
    ]
  },
  output: {
    filename: 'main-[contenthash:4]-bundle.js',
    path: path.resolve(__dirname,'../dist'),
    publicPath: '/'
  }
};

module.exports = merge.smartStrategy({'module.rules.use': 'prepend'})(common,test);
