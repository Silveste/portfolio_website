const merge = require('webpack-merge');
const common = require('./webpack.common');
const path = require('path');

const dev = {
  mode: 'development',
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          'style-loader'
        ]
      },
      {
        test: /\.(png|jpe?g|gif)$/,
        use: {
          loader: 'responsive-loader',
          options: {
            disable: true
          }
        },
        include: path.resolve(__dirname,'../src/assets/imgs')
      },
    ]
  },
  devServer: {
    overlay: true
  },
  devtool: 'source-map'
};

module.exports = merge.smartStrategy({'module.rules.use': 'prepend'})(common,dev);
