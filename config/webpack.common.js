const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackInlineSVGPlugin = require('html-webpack-inline-svg-plugin');
const path = require('path');

module.exports = {
  entry: './src/main-script.js',
  plugins: [
    new HtmlWebpackPlugin({
      title: 'Silveste | Antonio Fernández Portfolio',
      template: './src/index.pug'
    }),
    new HtmlWebpackInlineSVGPlugin({
      runPreEmit: true,
      svgoConfig: [
        { removeUselessDefs: false },
        { removeHiddenElems: false },
        { removeViewBox: false }
      ],
    })
  ],
  module: {
    rules: [
      {
        test: /\.cson$/,
        use: ['cson-loader'],
        include: path.resolve(__dirname,'../src/data')
      },
      {
        test: /\.js$/,
        use: {
          loader: 'babel-loader',
          options: {
            configFile: './config/babel.config.js'
          }
        },
        include: path.resolve(__dirname,'../src')
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: 'css-loader',
            options: {
              // Import is handled by postcss-import
              import: false
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              config: {path: __dirname + '/postcss.config.js'}
            }
          }
        ]
      },
      {
        test: /\.pug$/,
        use: ['pug-loader'],
        include: path.resolve(__dirname,'../src')
      },
      {
        test: /\.svg$/,
        use: {
          loader: 'file-loader',
          options: {
            name: '[name]-[contenthash:4].[ext]',
            outputPath: 'assets/imgs'
          }
        },
        include: path.resolve(__dirname,'../src/assets/imgs')
      },
      {
        test: /\.pdf$/,
        use: {
          loader: 'file-loader',
          options: {
            name: '[name]-[contenthash:4].[ext]',
            outputPath: 'assets/docs'
          }
        },
        include: path.resolve(__dirname,'../src/assets/docs')
      },
      {
        test: /\.(svg|ttf|eot|woff|woff2)$/,
        use: {
          loader: "file-loader",
          options: {
            name: '[name].[ext]',
            outputPath: 'assets/fonts',
          },
        },
        include: path.resolve(__dirname, '../src/assets/fonts'),
      },
    ],
  },
  resolve: {
    extensions: ['.js', '.css', '.cson', '.pug'],
  },
};
