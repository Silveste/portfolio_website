const postcssPresetEnv = require('postcss-preset-env');
const path = require('path');
const purgecss = require('@fullhuman/postcss-purgecss')({

  // Specify the paths to all of the template files in your project
  content: [
    './src/**/*.pug'
  ],
  // Include any special characters you're using in this regular expression
  defaultExtractor: content => content.match(/[\w-/:]+(?<!:)/g) || [],
  whitelistPatterns: [/^js-/]
})

module.exports = (ctx) => ({
  plugins: [
    require('postcss-import'),
    require('postcss-advanced-variables'),
    require('tailwindcss')(path.resolve(__dirname,'tailwind.config.js')),
    postcssPresetEnv({stage: 1}),
    ctx.webpack.mode === 'production' ? purgecss : null
  ]
});
