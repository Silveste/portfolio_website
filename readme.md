# Antonio Portfolio Website

This repository contains the source code of Antonio Fernandez @Silveste portfolio website.

You can find the live site [here](https://silveste.com)

Please feel free to copy anything and use the code in your own project. If you use something from here, no attribution is required, however it is always welcome.

## Repository structure
### /assets
Folder with project assets that are not included in the code (i.e. older versions or gimp files)
### /config
Config files for babel, postcss, tailwind and webpack.
### /docs
SVG files with wireframes and mockups.
### /src
Source code of the project.
